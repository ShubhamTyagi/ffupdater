package de.marmaro.krt.ffupdater;

import java.security.InvalidParameterException;
import java.util.Calendar;

/**
 * This class builds (depending on the architecture) the download url.
 * Furthermore this class can validate if the api level is high enough for running firefox.
 */
class DownloadUrl {
    private static final String NON_X86_ARCHITECTURE = "android";
    private static final String X86_ARCHITECTURE = "android-x86";

    private static final String URL_TEMPLATE = "https://download.mozilla.org/?product=fennec-latest&os=%s&lang=multi";
    private static final String BETA_URL_TEMPLAYE = "https://download.mozilla.org/?product=fennec-beta-latest&os=%s&lang=multi";

    //NIGHTLY BUILD HAS DIFFERENT URL
    private static final String NIGHTLY_URL_TEMPLATE = "https://archive.mozilla.org/pub/mobile/nightly/latest-mozilla-central-android-api-16/fennec-%s.multi.android-arm.apk";
    private static final String NIGHTLY_X86_URL_TEMPLATE = "https://archive.mozilla.org/pub/mobile/nightly/latest-mozilla-central-android-x86/fennec-%s.multi.android-i386.apk";

    //fenix download template
    private static final String FENIX_URL_TEMPLATE = "https://index.taskcluster.net/v1/task/";
    private static final String FENIX_NAMESPACE = "project.mobile.fenix.signed-nightly.nightly";
    private static final String FENIX_LATEST_BUILD = ".%s.%s.%s.latest/artifacts/"; //YEAR.MON.DATE
    private static final String FENIX_ARHITECTURE_APK = "public/target.%s.apk";

    //https://index.taskcluster.net/v1/task/project.mobile.fenix.signed-nightly.nightly.2019.4.9.latest/artifacts/public/target.arm.apk
    private String architecture;
    private int apiLevel;

    DownloadUrl(String architecture, int apiLevel) {
        this.architecture = architecture;
        this.apiLevel = apiLevel;
    }

    boolean isApiLevelSupported() {
        // https://support.mozilla.org/en-US/kb/will-firefox-work-my-mobile-device
        return (apiLevel >= 16);
    }

    String getArchitecture() {
        return architecture;
    }

    int getApiLevel() {
        return apiLevel;
    }

    String getUrl(String updateChannel, Version version) {
        if (0 == apiLevel || null == architecture) {
            throw new InvalidParameterException("Please call setApiLevel and setArchitecture before calling getUrl()");
        }
        // INFO: Update URI as specified in https://archive.mozilla.org/pub/mobile/releases/latest/README.txt
        String mozApiArch = getMozillaApiArchitecture();

        switch (updateChannel) {
            case UpdateChannel.STABLE:
                return String.format(URL_TEMPLATE, mozApiArch);
            case UpdateChannel.BETA:
                return String.format(BETA_URL_TEMPLAYE, mozApiArch);
            case UpdateChannel.NIGHTLY:
                if (mozApiArch.contentEquals(NON_X86_ARCHITECTURE)) {
                    return String.format(NIGHTLY_URL_TEMPLATE, version.get());
                } else {
                    return String.format(NIGHTLY_X86_URL_TEMPLATE, version.get());
                }

            case UpdateChannel.FENIX:
                return getFenixUrl(mozApiArch,0);
        }
        return String.format(URL_TEMPLATE, mozApiArch);
    }


    /**
     * @param mozApiArch api architecture
     * @param yesterdate whether chechk for yesterday build (values must be 0 or 1 )
     * @return fenix apk url
     */
    private String getFenixUrl(String mozApiArch,int yesterdate) {
        if (yesterdate>1||yesterdate<0)
            yesterdate=0;
        String APK_URL = FENIX_URL_TEMPLATE + FENIX_NAMESPACE;
        Calendar calendar = Calendar.getInstance();

        int year = calendar.get(Calendar.YEAR);
        int mon = calendar.get(Calendar.MONTH);
        int date = calendar.get(Calendar.DATE);
        APK_URL += String.format(FENIX_LATEST_BUILD, year, mon, date-yesterdate);

        if (mozApiArch.contentEquals(NON_X86_ARCHITECTURE))
            APK_URL += String.format(FENIX_ARHITECTURE_APK, "arm");
        else
            APK_URL += String.format(FENIX_ARHITECTURE_APK, "x86");

        return APK_URL;

    }

    private String getMozillaApiArchitecture() {
        if (architecture.equals("i686") || architecture.equals("x86_64")) {
            return X86_ARCHITECTURE;
        }
        return NON_X86_ARCHITECTURE;
    }
}
