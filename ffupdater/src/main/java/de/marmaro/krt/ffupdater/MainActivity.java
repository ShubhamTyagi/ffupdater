package de.marmaro.krt.ffupdater;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import de.marmaro.krt.ffupdater.background.LatestReleaseService;
import de.marmaro.krt.ffupdater.background.RepeatedNotifierExecuting;
import de.marmaro.krt.ffupdater.settings.SettingsActivity;

public class MainActivity extends AppCompatActivity {

    public static final String OPENED_BY_NOTIFICATION = "OpenedByNotification";
    private static final String TAG = "MainActivity";
    private static final String PROPERTY_OS_ARCHITECTURE = "os.arch";
    private static String mDownloadUrl = "";

    protected TextView firefoxAvailableVersionTextview, firefoxInstalledVersionTextView, subtitleTextView;
    protected FloatingActionButton downloadFirefox;
    protected Toolbar toolbar;
    protected ProgressBar progressBar;
    protected SwipeRefreshLayout swipeRefreshLayout;

    private DownloadUrl downloadUrlObject;
    private SharedPreferences sharedPref;
    private FirefoxMetadata localFirefox;
    private Version availableVersion;

    /**
     * Listen to the broadcast from {@link LatestReleaseService} and use the transmitted {@link Version} object.
     */
    private BroadcastReceiver latestReleaseServiceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Version version = (Version) intent.getSerializableExtra(LatestReleaseService.EXTRA_RESPONSE_VERSION);
            setAvailableVersion(version);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        UpdateChannel.channel = sharedPref.getString(getString(R.string.pref_build), "version");
        initUI();
        initUIActions();

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build();
        StrictMode.setThreadPolicy(policy);

        downloadUrlObject = new DownloadUrl(System.getProperty(PROPERTY_OS_ARCHITECTURE), android.os.Build.VERSION.SDK_INT);

        // starts the repeated update check
        RepeatedNotifierExecuting.register(this);

        if (!downloadUrlObject.isApiLevelSupported()) {
            Log.e(TAG, "android-" + downloadUrlObject.getApiLevel() + " is not supported.");
            showAndroidTooOldError();
        }

    }

    private void initUIActions() {
        downloadFirefox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                mDownloadUrl = downloadUrlObject.getUrl(UpdateChannel.channel, availableVersion);
                i.setData(Uri.parse(mDownloadUrl));
                startActivity(i);
            }
        });
        //set to listen pull down of screen
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadLatestMozillaVersion();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void initUI() {
        setContentView(R.layout.main_avticity);
        firefoxAvailableVersionTextview = findViewById(R.id.firefox_available_version);
        firefoxInstalledVersionTextView = findViewById(R.id.firefox_installed_version);
        subtitleTextView = findViewById(R.id.toolbar_subtitle);
        toolbar = findViewById(R.id.toolbar);
        downloadFirefox = findViewById(R.id.fab);
        downloadFirefox.hide();
        swipeRefreshLayout = findViewById(R.id.swipeContainer);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright, android.R.color.holo_green_light, android.R.color.holo_orange_light, android.R.color.holo_red_light);
        progressBar = findViewById(R.id.progress_wheel);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(latestReleaseServiceReceiver, new IntentFilter(LatestReleaseService.RESPONSE_ACTION));
        UpdateChannel.channel = sharedPref.getString(getString(R.string.pref_build), "version");

        switch (UpdateChannel.channel) {
            case UpdateChannel.STABLE:
                subtitleTextView.setText(getString(R.string.firefox_for_android));
                break;
            case UpdateChannel.BETA:
                subtitleTextView.setText(getString(R.string.firefox_for_android_beta));
                break;
            case UpdateChannel.NIGHTLY:
                subtitleTextView.setText(getString(R.string.firefox_nightly_for_developer));
                break;
            case UpdateChannel.FENIX:
                subtitleTextView.setText(getString(R.string.firefox_fenix));
                break;
        }

        // load latest firefox version, when intent has got the "open by notification" flag
        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            if (bundle.getBoolean(OPENED_BY_NOTIFICATION, false)) {
                bundle.putBoolean(OPENED_BY_NOTIFICATION, false);
                getIntent().replaceExtras(bundle);
                loadLatestMozillaVersion();
            }
        }

        // check for the version of the current installed firefox
        localFirefox = new FirefoxMetadata.Builder().checkLocalInstalledFirefox(getPackageManager());
        // log and display the current firefox version
        if (localFirefox.isInstalled()) {
            String format = "Firefox %s (%s) is installed.";
            Log.i(TAG, String.format(format, localFirefox.getVersionName(), localFirefox.getVersionCode()));
        } else {
            Log.i(TAG, "Firefox is not installed.");
        }
        //check for  latest version as soon as app instance is created: this will remove redundancy of check availability button
        loadLatestMozillaVersion();
        displayVersions();
    }

    /**
     * Display the version number of the latest firefox release.
     *
     * @param value version of the latest firefox release
     */
    private void setAvailableVersion(Version value) {
        if (value == null) {
            firefoxAvailableVersionTextview.setVisibility(View.GONE);
            (new AlertDialog.Builder(this))
                    .setMessage(getString(R.string.check_available_error_message))
                    .setPositiveButton(getString(R.string.ok), null)
                    .show();
        } else {
            availableVersion = value;
            Log.d(TAG, "Found highest available version: " + availableVersion.get());
            progressBar.setVisibility(View.GONE);
            downloadFirefox.show();
            displayVersions();
        }
    }

    /**
     * Refresh the installedVersionTextView and availableVersionTextView
     */
    private void displayVersions() {
        String installedText;
        if (localFirefox.isInstalled()) {
            String format = getString(R.string.installed_version_text_format);
            installedText = String.format(format, localFirefox.getVersionName(), localFirefox.getVersionCode());
        } else {
            String format = getString(R.string.not_installed_text_format);
            installedText = String.format(format, getString(R.string.none), getString(R.string.not_installed));
        }
        firefoxInstalledVersionTextView.setText(installedText);
        String availableText;
        if (null == availableVersion) {
            availableText = "";
        } else {
            availableText = availableVersion.get();
        }
        firefoxAvailableVersionTextview.setText(availableText);
    }

    /**
     * Set the availableVersionTextView to "(checking…)" and start the LatestReleaseService service
     */
    private void loadLatestMozillaVersion() {
        firefoxAvailableVersionTextview.setVisibility(View.VISIBLE);
        firefoxAvailableVersionTextview.setText(getString(R.string.checking));
        progressBar.setVisibility(View.VISIBLE);
        if (isConnectedToInternet()) {
            Intent checkVersions = new Intent(this, LatestReleaseService.class);
            progressBar.setVisibility(View.VISIBLE);
            startService(checkVersions);
        } else {
            Snackbar.make(findViewById(R.id.coordinatorLayout), R.string.not_connected_to_internet, Snackbar.LENGTH_LONG).show();
            progressBar.setVisibility(View.GONE);
        }
    }

    /**
     * Display an error that the user uses a version which is not longer unsupported.
     */
    private void showAndroidTooOldError() {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(getString(R.string.firefox_version_is_too_old));
        alertDialog.setMessage(getString(R.string.firefox_needs_at_least_android_41));
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(latestReleaseServiceReceiver);
    }

    private boolean isConnectedToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        if ((connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null
                && connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED)
                || (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null
                && connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED))
            return true;
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_about:
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle(getString(R.string.about));
                alertDialog.setMessage(getString(R.string.infobox));
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
                break;
            case R.id.action_settings:
                //start settings activity where we use select firefox product and release type;
                startActivity(new Intent(this, SettingsActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
